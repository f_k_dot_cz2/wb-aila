const helpers = {
    appendVariables(template) {                               
        let regex = /\[%(\w+)%\]/g;

        let templateUpdated = template.replace(regex, function (match, p1) { 
            return process.env[p1] || p1;        
        });
        
        return templateUpdated;
    }
};

module.exports = helpers;